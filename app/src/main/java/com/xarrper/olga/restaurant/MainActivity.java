package com.xarrper.olga.restaurant;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button calculateButton;
    private EditText sumEditText;
    private EditText percentEditText;
    private TextView tipTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sumEditText = (EditText) findViewById(R.id.sumEditText);
        percentEditText = (EditText) findViewById(R.id.percentEditText);
        tipTextView = (TextView) findViewById(R.id.tipEditText);


        calculateButton = (Button) findViewById(R.id.calculateButton);
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Double sum = Double.valueOf(sumEditText.getText().toString());
                    Double percent = Double.valueOf(percentEditText.getText().toString());

                    Double tip = (sum * percent) / 100;

                    tipTextView.setText(String.format("%.2f", tip));
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, getString(R.string.error), Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
